// [[Rcpp::plugins("cpp11")]]

#include <Rcpp.h>

using namespace Rcpp;

//' @export
// [[Rcpp::export]]
NumericVector rolling_mean(const NumericVector& x, const size_t window) {
    const size_t n = x.size();
    NumericVector res = no_init(n - window + 1);
    double summed = std::accumulate(x.begin(), x.begin() + window, 0.0);
    res[0] = summed / window;
    for (size_t i = window; i < n; ++i) {
        summed += x[i] - x[i - window];
        res[i - window + 1] = summed / window;
    }
    return res;
}
