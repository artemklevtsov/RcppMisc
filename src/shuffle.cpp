// [[Rcpp::plugins(cpp11)]]

#include <Rcpp.h>

using namespace Rcpp;

template <int RTYPE>
Vector<RTYPE> shuffle_impl(const Vector<RTYPE>& x) {
    Vector<RTYPE> res = no_init(x.size());
    std::copy(x.begin(), x.end(), res.begin());
    auto first = res.begin(), last = res.end();
    auto n = last - first;
    for (auto i = n - 1; i > 0; --i) {
        size_t r = i * unif_rand();
        std::swap(*(first + i), *(first + r));
    }
    return res;
}

//' @title Fisher-Yates Shuffle of the vector
//' @param x A vector of one or more elements.
//' @return A vector of length size with elements drawn from either `x`.
//' @export
// [[Rcpp::export]]
SEXP shuffle(SEXP x) {
    RCPP_RETURN_VECTOR(shuffle_impl, x);
}
