#include <Rcpp.h>

using namespace Rcpp;

//' @export
// [[Rcpp::export]]
IntegerVector tabulate2(const IntegerVector& x, const size_t nbins) {
    IntegerVector counts(nbins);
    size_t n = x.size();
    for (size_t i = 0; i < n; i++) {
        if (x[i] > 0 && x[i] <= nbins)
            counts[x[i] - 1]++;
    }
    return counts;
}
