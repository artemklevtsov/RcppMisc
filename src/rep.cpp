// [[Rcpp::plugins(cpp11)]]

#include <Rcpp.h>

using namespace Rcpp;

template <int RTYPE>
Vector<RTYPE> rep_each_impl(const Vector<RTYPE>& x, size_t each) {
    size_t n = x.size();
    Vector<RTYPE> res = no_init(n * each);
    auto begin = res.begin();
    for (size_t i = 0; i < n; begin += each, ++i)
        std::fill(begin, begin + each, x[i]);
    return res;
}

//' @export
// [[Rcpp::export]]
SEXP rep_each(SEXP x, size_t each) {
    RCPP_RETURN_VECTOR(rep_each_impl, x, each);
}

template <int RTYPE>
Vector<RTYPE> rep_times_impl(const Vector<RTYPE>& x, size_t times) {
    size_t n = x.size();
    Vector<RTYPE> res = no_init(n * times);
    auto begin = x.begin(), end = x.end();
    auto dest = res.begin();
    for (size_t i = 0; i < times; dest += n, ++i)
        std::copy(begin, end, dest);
    return res;
}

//' @export
// [[Rcpp::export]]
SEXP rep_times(SEXP x, size_t times) {
    RCPP_RETURN_VECTOR(rep_times_impl, x, times);
}

template <int RTYPE>
Vector<RTYPE> rep_int_impl(const Vector<RTYPE>& x, const IntegerVector& times) {
    size_t n = x.size(), ntimes = times.size();
    if (n == ntimes) {
        size_t n_out = std::accumulate(times.begin(), times.end(), 0);
        Vector<RTYPE> res = no_init(n_out);
        auto begin = res.begin();
        for (size_t i = 0; i < n; begin += times[i], ++i)
            std::fill(begin, begin + times[i], x[i]);
        return res;
    } else if (ntimes == 1)
        return rep_times_impl(x, times[0]);
    else
        std::invalid_argument("Invalid 'times' value");
    return R_NilValue;
}

//' @export
// [[Rcpp::export]]
SEXP rep_int(SEXP x, IntegerVector times) {
    RCPP_RETURN_VECTOR(rep_int_impl, x, times);
}

// [[Rcpp::export]]
List rep_list(const List& x, size_t times) {
    size_t n = x.size();
    List res(n);
    for (size_t i = 0; i < n; ++i)
        res[i] = rep_times(x[i], times);
    res.names() = x.names();
    return res;
}
