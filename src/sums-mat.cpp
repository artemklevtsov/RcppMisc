// [[Rcpp::plugins(cpp11)]]

// [[Rcpp::depends(RcppParallel)]]
#include <RcppParallel.h>
#include <Rcpp.h>

using namespace Rcpp;
using namespace RcppParallel;

struct ColSums : Worker {
    const RMatrix<double> input;
    RVector<double> output;

    ColSums(const NumericMatrix input, NumericVector output):
        input(input), output(output) {}

    void operator()(size_t begin, size_t end) {
        for (size_t i = begin; i < end; ++i) {
            auto tmp = input.column(i);
            output[i] = std::accumulate(tmp.begin(), tmp.end(), 0.0);
        }
    }
};

struct RowSums : Worker {
    const RMatrix<double> input;
    RVector<double> output;

    RowSums(const NumericMatrix input, NumericVector output):
        input(input), output(output) {}

    void operator()(size_t begin, size_t end) {
        for (size_t i = begin; i < end; ++i) {
            auto tmp = input.column(i);
            output[i] = std::accumulate(tmp.begin(), tmp.end(), 0.0);
        }
    }
};

//' @export
// [[Rcpp::export]]
NumericVector col_sums(const NumericMatrix& x) {
    size_t n = x.cols();
    NumericVector res = no_init(n);
    ColSums colSums(x, res);
    parallelFor(0, n, colSums);
    res.attr("names") = colnames(x);
    return res;
}

//' @export
// [[Rcpp::export]]
NumericVector row_sums(const NumericMatrix& x) {
    size_t n = x.rows();
    NumericVector res = no_init(n);
    RowSums rowSums(x, res);
    parallelFor(0, n, rowSums);
    res.attr("names") = rownames(x);
    return res;
}
