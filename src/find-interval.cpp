// [[Rcpp::plugins(cpp11)]]

#include <Rcpp.h>

using namespace Rcpp;

//' @export
// [[Rcpp::export]]
IntegerVector find_interval(const NumericVector& x, const NumericVector& breaks) {
    size_t n = x.size();
    IntegerVector out = no_init(n);
    auto it = breaks.begin(), end = breaks.end();
    for (size_t i = 0; i < n; ++i) {
        auto ubound = std::upper_bound(it, end, x[i]);
        out[i] = std::distance(it, ubound);
    }
    return out;
}
