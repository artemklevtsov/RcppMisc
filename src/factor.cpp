#include <Rcpp.h>

using namespace Rcpp;

template <int RTYPE>
IntegerVector as_factor_impl(const Vector<RTYPE> & x) {
    Vector<RTYPE> lvls = sort_unique(x);
    IntegerVector out = match(x, lvls);
    out.attr("levels") = as<CharacterVector>(lvls);
    out.attr("class") = "factor";
    return out;
}

//' @export
// [[Rcpp::export]]
SEXP as_factor(SEXP x) {
    if (Rf_isFactor(x)) return x;
    switch( TYPEOF(x) ) {
    case INTSXP: return as_factor_impl<INTSXP>(x);
    case REALSXP: return as_factor_impl<REALSXP>(x);
    case STRSXP: return as_factor_impl<STRSXP>(x);
    default: stop("Unsupported type.");
    }
    return R_NilValue;
}
