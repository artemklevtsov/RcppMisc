// [[Rcpp::plugins("cpp11")]]
// [[Rcpp::depends("BH")]]

#include <Rcpp.h>
#include <boost/asio.hpp>

using namespace Rcpp;
using namespace boost::asio;
using namespace boost::asio::ip;

//' @export
// [[Rcpp::export]]
std::vector<std::string> gethostbyname(std::string hostname) {
    std::vector<std::string> addresses;
    io_service service;
    tcp::resolver resolver(service);
    tcp::resolver::query query(hostname, "");
    tcp::resolver::iterator it = resolver.resolve(query), end;
    tcp::endpoint endpoint;
    while (it != end) {
        endpoint = *it++;
        addresses.push_back(endpoint.address().to_string());
    }
    return addresses;
}

//' @export
// [[Rcpp::export]]
std::vector<std::string> gethostbyaddr(std::string address) {
    std::vector<std::string> hostnames;
    tcp::endpoint endpoint;
    io_service service;
    address_v4 ip = ip::address_v4::from_string(address);
    endpoint.address(ip);
    tcp::resolver resolver(service);
    tcp::resolver::iterator it = resolver.resolve(endpoint), end;
    for (size_t i = 1; it != end; ++it, ++i)
        hostnames.push_back(it->host_name());
    return(hostnames);
}
