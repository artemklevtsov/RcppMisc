// [[Rcpp::plugins("cpp11")]]
// [[Rcpp::depends("BH")]]

#include <Rcpp.h>
#include <boost/sort/spreadsort/spreadsort.hpp>

using namespace Rcpp;
using namespace boost::sort::spreadsort;

template <int RTYPE, typename CTYPE = typename Vector<RTYPE>::stored_type>
Vector<RTYPE> sort_impl(const Vector<RTYPE> & x, bool decreasing = false) {
    std::vector<CTYPE> res(x.size());
    std::copy(x.begin(), x.end(), res.begin());
    spreadsort(res.begin(), res.end());
    if (decreasing) std::reverse(res.begin(), res.end());
    return wrap(res);
}

template <>
Vector<STRSXP> sort_impl(const Vector<STRSXP> & x, bool decreasing) {
    std::vector<std::string> res(x.size());
    std::copy(x.begin(), x.end(), res.begin());
    spreadsort(res.begin(), res.end());
    if (decreasing) std::reverse(res.begin(), res.end());
    return wrap(res);
}

//' @export
// [[Rcpp::export]]
SEXP sort2(SEXP x, bool decreasing = false) {
    switch(TYPEOF(x)) {
    case INTSXP: return sort_impl<INTSXP>(x, decreasing);
    case REALSXP: return sort_impl<REALSXP>(x, decreasing);
    case STRSXP: return sort_impl<STRSXP>(x, decreasing);
    default: stop("Unsupported type.");
    }
}
