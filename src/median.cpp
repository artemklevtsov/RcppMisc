#include <Rcpp.h>

using namespace Rcpp;

// [[Rcpp::export]]
double median_rcpp(const NumericVector &x) {
    NumericVector y(x);
    size_t n = y.size();
    size_t half = n / 2;
    std::nth_element(y.begin(), y.begin() + half, y.end());
    if (n % 2 == 1) {
        return *(y.begin() + half);
    } else {
        const double val1 = *(y.begin() + half);
        const double val2 = *(std::max_element(y.begin(), y.begin() + half));
        return (val1 + val2) / 2.0;
    }
}
