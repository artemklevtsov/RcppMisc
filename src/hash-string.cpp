// [[Rcpp::plugins("cpp11")]]

#include <Rcpp.h>
#include <functional>
using namespace Rcpp;


//' @export
// [[Rcpp::export]]
std::vector<size_t> hash(const std::vector<std::string>& x) {
    std::vector<size_t> res(x.size());
    std::transform(x.begin(), x.end(), res.begin(), std::hash<std::string>());
    return res;
}
