#' @export
rep.data.frame <- function(x, times) {
    rnames <- attr(x, "row.names")
    x <- rep_list(x, times)
    class(x) <- "data.frame"
    if (is.character(rnames))
        attr(x, "row.names") <- make.unique(rep.int(rnames, times))
    else
        attr(x, "row.names") <- .set_row_names(length(rnames) * times)
    return(x)
}
